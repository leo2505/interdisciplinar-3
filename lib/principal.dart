import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:interdisciplinar_app/novo_chamado.dart';
import 'package:interdisciplinar_app/tela_chamado.dart';

import 'configuracoes.dart';

class Principal extends StatefulWidget {
  @override
  _PrincipalState createState() => _PrincipalState();
}

class _PrincipalState extends State<Principal> {
  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    return DefaultTabController(
      // Added
      length: 3, // Added
      initialIndex: 0, //Added
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          bottom: TabBar(
            indicatorColor: Colors.pinkAccent,
            tabs: [
              Tab(
                child: Text(
                  "Abertos",
                  style: TextStyle(color: Colors.white),
                ),
              ),
              Tab(
                child: Text(
                  "Pendentes",
                  style: TextStyle(color: Colors.white),
                ),
              ),
              Tab(
                child: Text(
                  "Concluídos",
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          ),
          /* title: Text("ITIL Help",
              style: TextStyle(
                color: Colors.blue,
              ),
          ),*/
          elevation: 0.0,
          //backgroundColor: Colors.transparent,
          backgroundColor: Color(0xFFd80645),
          actions: <Widget>[
            Expanded(
              flex: 2, // 60%
              child: Column(
                children: <Widget>[
                  FlatButton(
                    child: Icon(
                      Icons.more_vert,
                      color: Colors.white,
                      size: 24.0,
                      semanticLabel: 'Configurações',
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Configuracoes()),
                      );
                    },
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 6, // 60%
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(15.0),
                    alignment: FractionalOffset(-0.13, 0.0),
                    child: Text(
                      "ITIL Help",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w600),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 2, // 60%
              child: Column(
                children: <Widget>[
                  FlatButton(
                    child: Icon(
                      Icons.add,
                      color: Colors.white,
                      size: 24.0,
                      semanticLabel: 'Novo Chamado',
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => NovoChamado()),
                      );
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
        body: TabBarView(
          children: [pagina1(), pagina2(), pagina3()],
        ),
      ),
    );
  }

  pagina1() {

    return Container(
      // Add box decoration
        decoration: BoxDecoration(
          // Box decoration takes a gradient
          gradient: LinearGradient(
            // Where the linear gradient begins and ends
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            // Add one stop for each color. Stops should increase from 0 to 1
            colors: [
              // Colors are easy thanks to Flutter's Colors class.
              Color(0xFFd80645),
              Color(0xFFfb6c40),
            ],
          ),
        ),
        child: Container(

          padding: EdgeInsets.all(0),
          //color: Colors.white,
          child: StreamBuilder(
            stream: Firestore.instance
                .collection("chamado")
                .where("status", isEqualTo: 1)
                .orderBy("dataAbertura")
                .snapshots(),
            builder: (context, snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.none:
                case ConnectionState.done:
                case ConnectionState.waiting:
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                default:
                  if (snapshot.data.documents.length == 0) {
                    //
                    return Center(
                      child: Text(
                        "Não há chamados abertos! ",
                        // + snapshot.error.toString(),
                        style: TextStyle(color: Colors.white70, fontSize: 20),
                      ),
                    );
                  }

                  return ListView.builder(
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (context, index) {
                        return Card(
                          elevation: 0,
                          color: Color.fromRGBO(255, 255, 255, 0.25),
                          // Lista os itens
                          child: Container(
                            child: ListTile(
                              //snapshot.data.documents[index].documentID.toString() - pega o ID
                              title: Text(
                                  snapshot.data.documents[index]
                                      .data["itemGUT"],
                                  style:
                                  TextStyle(fontSize: 18, color: Colors.white)),
                              subtitle: Text(
                                  "Prioridade: " +
                                      snapshot.data.documents[index]
                                          .data["mediaGeralFinal"].toString(),
                                  style:
                                  TextStyle(fontSize: 13, color: Colors.white)),
                              trailing: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  IconButton(
                                    icon: Icon(
                                      Icons.remove_red_eye,
                                      //color: Colors.blue,
                                      color: Colors.white,
                                      size: 24.0,
                                      semanticLabel: 'visualizar',
                                    ),
                                    color: Colors.white,
                                    onPressed: () {
                                      print('documento: ' +
                                          snapshot.data.documents[index]
                                              .toString());
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                TelaChamado(
                                                    snapshot.data
                                                        .documents[index])),
                                      );
                                    },
                                    //onPressed: () {
                                    //Navigator.push(
                                    //    context, MaterialPageRoute(builder: (context) => FilmesEditar("alt",snapshot.data.documents[index])));
                                    //},
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      });
              }
            },
          ),
        ),
    );
  }

  pagina2() {
    return Container(
        // Add box decoration
        decoration: BoxDecoration(
        // Box decoration takes a gradient
        gradient: LinearGradient(
        // Where the linear gradient begins and ends
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        // Add one stop for each color. Stops should increase from 0 to 1
        colors: [
        // Colors are easy thanks to Flutter's Colors class.
        Color(0xFFd80645),
    Color(0xFFfb6c40),
    ],
    ),
    ),
    child: Container(
      //color: Color(0xFFd80645),
      padding: EdgeInsets.all(0),
      //color: Colors.white,
      child: StreamBuilder(
        stream: Firestore.instance
            .collection("chamado")
            .where("status", isEqualTo: 2)
            .orderBy("mediaGeralFinal")
            .snapshots(),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.done:
            case ConnectionState.waiting:
              return Center(
                child: CircularProgressIndicator(),
              );
            default:
              if (snapshot.data.documents.length == 0) {
                //
                return Center(
                  child: Text(
                    "Não há chamdos pendentes! ",
                    // + snapshot.error.toString(),
                    style: TextStyle(color: Colors.white70, fontSize: 20),
                  ),
                );
              }

              return ListView.builder(
                  itemCount: snapshot.data.documents.length,
                  itemBuilder: (context, index) {
                    return Card(
                      elevation: 0,
                      color: Color.fromRGBO(255, 255, 255, 0.25),
                      // Lista os itens
                      child: Container(
                        child: ListTile(
                          //snapshot.data.documents[index].documentID.toString() - pega o ID
                          title: Text(
                              snapshot.data.documents[index].data["itemGUT"],
                              style:
                              TextStyle(fontSize: 18, color: Colors.white)),
                          subtitle: Text(
                              "Prioridade: " +
                                  snapshot.data.documents[index]
                                      .data["mediaGeralFinal"].toString() +
                                  " / " + snapshot.data.documents[index]
                                  .data["nomestatus"].toString(),
                              style:
                              TextStyle(fontSize: 13, color: Colors.white)),
                          trailing: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              IconButton(
                                icon: Icon(
                                  Icons.remove_red_eye,
                                  color: Colors.white,
                                  size: 24.0,
                                  semanticLabel: 'visualizar',
                                ),
                                color: Colors.white,
                                onPressed: () {
                                  print('documento: ' +
                                      snapshot.data.documents[index]
                                          .toString());
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            TelaChamado(
                                                snapshot.data
                                                    .documents[index])),
                                  );
                                },
                                //onPressed: () {
                                //Navigator.push(
                                //    context, MaterialPageRoute(builder: (context) => FilmesEditar("alt",snapshot.data.documents[index])));
                                //},
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  });
          }
        },
      ),
    ),
    );
  }

  pagina3() {
    return Container(
        // Add box decoration
        decoration: BoxDecoration(
        // Box decoration takes a gradient
        gradient: LinearGradient(
        // Where the linear gradient begins and ends
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        // Add one stop for each color. Stops should increase from 0 to 1
        colors: [
        // Colors are easy thanks to Flutter's Colors class.
        Color(0xFFd80645),
    Color(0xFFfb6c40),
    ],
    ),
    ),
    child:
    Container(
      //color: Color(0xFFd80645),
      padding: EdgeInsets.all(0),
      //color: Colors.white,
      child: StreamBuilder(
        stream: Firestore.instance
            .collection("chamado")
            .where("status", isEqualTo: 3)
            .orderBy("mediaGeralFinal")
            .snapshots(),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.done:
            case ConnectionState.waiting:
              return Center(
                child: CircularProgressIndicator(),
              );
            default:
              if (snapshot.data.documents.length == 0) {
                //
                return Center(
                  child: Text(
                    "Não há chamados concluídos! ",
                    // + snapshot.error.toString(),
                    style: TextStyle(color: Colors.white70, fontSize: 20),
                  ),
                );
              }

              return ListView.builder(
                  itemCount: snapshot.data.documents.length,
                  itemBuilder: (context, index) {
                    return Card(
                      // Lista os itens
                      elevation: 0,
                      color: Color.fromRGBO(255, 255, 255, 0.25),
                      child: Container(
                        child: ListTile(
                          //snapshot.data.documents[index].documentID.toString() - pega o ID
                          title: Text(
                              snapshot.data.documents[index].data["itemGUT"],
                              style:
                              TextStyle(fontSize: 18, color: Colors.white)),
                          subtitle: Text(
                              "Prioridade: " +
                                  snapshot.data.documents[index]
                                      .data["mediaGeralFinal"].toString() +
                                  " / " + snapshot.data.documents[index]
                                  .data["nomestatus"].toString(),
                              style:
                              TextStyle(fontSize: 13, color: Colors.white)),
                          trailing: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              IconButton(
                                icon: Icon(
                                  Icons.remove_red_eye,
                                  color: Colors.white,
                                  size: 24.0,
                                  semanticLabel: 'visualizar',
                                ),
                                color: Colors.white,
                                onPressed: () {
                                  print('documento: ' +
                                      snapshot.data.documents[index]
                                          .toString());
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            TelaChamado(
                                                snapshot.data
                                                    .documents[index])),
                                  );
                                },
                                //onPressed: () {
                                //Navigator.push(
                                //    context, MaterialPageRoute(builder: (context) => FilmesEditar("alt",snapshot.data.documents[index])));
                                //},
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  });
          }
        },
      ),
    ),);
  }
}
