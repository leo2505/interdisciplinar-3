import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NovoItemGut extends StatefulWidget {
  @override
  _NovoItemGutState createState() => _NovoItemGutState();
}

class _NovoItemGutState extends State<NovoItemGut> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController nomeItem = TextEditingController();
  TextEditingController nomeCategoria = TextEditingController();
  TextEditingController gravidade = TextEditingController();
  TextEditingController tendencia = TextEditingController();
  TextEditingController urgencia = TextEditingController();
  var valueClassificacao;
  var idCategoria;
  var tiposClassificacao = [
    "Computador",
    "Rede",
    "Periféricos",
    "Impressoras",
    "Móveis"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //backgroundColor: Color(0xFFd80645),
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.white, //change your color here
          ),
          title: Text("Novo Item",
              style: TextStyle(
                color: Colors.white,
              )),
          elevation: 0.0,
          backgroundColor: Color(0xFFd80645),
          actions: <Widget>[
            /*FlatButton(
              child: Icon(
                Icons.add,
                color: Colors.blue,
                size: 24.0,
                semanticLabel: 'Novo Chamado',
              ),
              onPressed: () {},
            )*/
          ],
        ),
        body: novoItem());
  }

  novoItem() {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.all(16),
        // Add box decoration
        decoration: BoxDecoration(
          // Box decoration takes a gradient
          gradient: LinearGradient(
            // Where the linear gradient begins and ends
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            // Add one stop for each color. Stops should increase from 0 to 1
            colors: [
              // Colors are easy thanks to Flutter's Colors class.
              Color(0xFFd80645),
              Color(0xFFfb6c40),
            ],
          ),
        ),
        child: Container(
          padding: EdgeInsets.only(top: 5, left: 10, right: 10, bottom: 25),
          child: Form(
              key: formKey,
              child: Card(
                child: Container(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      TextFormField(
                        style: TextStyle(color: Colors.black),
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                            icon: Icon(
                              Icons.account_balance,
                              color: Colors.red,
                            ),
                            labelText: "Nome do item",
                            labelStyle: TextStyle(color: Colors.black)),
                        controller: nomeItem,
                        validator: (valor) {
                          if (valor.isEmpty) {
                            return "Informe o nome do item";
                          }
                          return null;
                        },
                      ),
                      Container(
                          margin: EdgeInsets.only(top: 20, bottom: 20),
                          padding: EdgeInsets.only(left: 40),
                          child: Column(
                            children: <Widget>[
                              DropdownButton<String>(
                                items: tiposClassificacao
                                    .map((String dropDownStringItem) {
                                  return DropdownMenuItem<String>(
                                    value: dropDownStringItem,
                                    child: Text(dropDownStringItem),
                                  );
                                }).toList(),
                                hint: Text(
                                  "Selecione uma categoria",
                                  style: TextStyle(color: Colors.black),
                                ),
                                onChanged: (value) {
                                  setState(() {
                                    valueClassificacao = value;
                                  });
                                },
                                isExpanded: true,
                                icon: Icon(
                                  Icons.category,
                                  color: Colors.red,
                                ),
                                value: valueClassificacao,
                                elevation: 2,
                                iconSize: 20.0,
                              ),
                            ],
                          )),
                      TextFormField(
                        maxLength: 1,
                        style: TextStyle(color: Colors.black),
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            icon: Icon(
                              Icons.priority_high,
                              color: Colors.red,
                            ),
                            labelText: "Gravidade",
                            labelStyle: TextStyle(color: Colors.black)),
                        controller: gravidade,
                        validator: (valor) {
                          if (valor.isEmpty) {
                            return "Obrigatório";
                          }
                          return null;
                        },
                      ),
                      TextFormField(
                        maxLength: 1,
                        style: TextStyle(color: Colors.black),
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            icon: Icon(
                              Icons.priority_high,
                              color: Colors.red,
                            ),
                            labelText: "Urgência",
                            labelStyle: TextStyle(color: Colors.black)),
                        controller: urgencia,
                        validator: (valor) {
                          if (valor.isEmpty) {
                            return "Obrigatório";
                          }
                          return null;
                        },
                      ),
                      TextFormField(
                        maxLength: 1,
                        style: TextStyle(color: Colors.black),
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            icon: Icon(
                              Icons.priority_high,
                              color: Colors.red,
                            ),
                            labelText: "Tendência",
                            labelStyle: TextStyle(color: Colors.black)),
                        controller: tendencia,
                        validator: (valor) {
                          if (valor.isEmpty) {
                            return "Obrigatório";
                          }
                          return null;
                        },
                      ),
                      RaisedButton(
                        color: Colors.indigo,
                        textColor: Colors.white,
                        child: Text("CADASTRAR"),
                        onPressed: () {
                          int gravidadeint = int.parse(gravidade.toString());
                          int urgenciaint = int.parse(urgencia.toString());
                          int tendenciaint = int.parse(tendencia.toString());
                          var totalItemGut =
                          (gravidadeint * urgenciaint * tendenciaint);
                          if (formKey.currentState.validate()) {
                            //cadastrarItem();
                            if (valueClassificacao == "Computador") {
                              idCategoria = "-LtgGvRcdg-aZ2P4BTVK";
                            } else if (valueClassificacao == "Rede") {
                              idCategoria = "-LtgHPHiAzcZfxvuFe3A";
                            } else if (valueClassificacao == "Periféricos") {
                              idCategoria = "-LtgHiL0YgmTbjQ2jFb3";
                            } else if (valueClassificacao == "Impressoras") {
                              idCategoria = "ExM0mwB6BOcecfLXnEtH";
                            } else if (valueClassificacao == "Móveis") {
                              idCategoria = "skxfL3GG6l9BwXKSuumT";
                            }
                            Firestore.instance.collection("itensGUT").add({
                              "nomeItem": nomeItem.text,
                              "idCategoriaItem": idCategoria,
                              "nomeCategoria": valueClassificacao,
                              "gravidade": int.parse(gravidade.toString()),
                              "urgencia": int.parse(urgencia.toString()),
                              "tendencia": int.parse(tendencia.toString()),
                              "totalItemGut": totalItemGut,
                            });
                            Navigator.pop(context);
                          }
                        },
                      ),
                    ],
                  ),
                )
              )),
        ),
      ),
    );
  }
}
