import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class NovoChamado extends StatefulWidget {
  @override
  _NovoChamadoState createState() => _NovoChamadoState();
}

class _NovoChamadoState extends State<NovoChamado> {
  final databaseReference = Firestore.instance;
  PageController pgController = PageController(
    initialPage: 0,
  );
  var queryCat;
  var _category;
  var valueClassificacao;
  var tiposClassificacao = ["Computador", "Rede", "Periféricos", "Impressoras"];
  var iditem;
  var idCategoria;
  var idCategoriaItem;
  var gravidadeItem;
  var urgenciaItem;
  var tendenciaItem;


  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  //TextEditingController categoriaItem = TextEditingController();
  TextEditingController nomeItem = TextEditingController();
  TextEditingController comentario = TextEditingController();

  TextEditingController gravidadeChamado = TextEditingController();
  TextEditingController tendenciaChamado = TextEditingController();
  TextEditingController urgenciaChamado = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.white, //change your color here
          ),
          title: Text("Novo Chamado",
              style: TextStyle(
                color: Colors.white,
              )),
          elevation: 0.0,
          backgroundColor: Color(0xFFd80645),
          actions: <Widget>[],
        ),
        body: telaNovoChamado());
  }

  telaNovoChamado() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Column(
          children: <Widget>[
            Center(
                child: Container(
                  margin: EdgeInsets.only(top:40),
              padding: EdgeInsets.only(top: 30,left: 40, right: 40, bottom: 30),
              child: Text(
                "Você está abrindo um novo chamado selecione abaixo o item com problema\nPasso 1 de 2",
                textAlign: TextAlign.center,
              ),
            )),
            Center(
              child: Container(
                  margin: EdgeInsets.all(10),
                  padding: EdgeInsets.all(40),
                  child: Column(
                    children: <Widget>[
                      DropdownButton<String>(
                        items:
                            tiposClassificacao.map((String dropDownStringItem) {
                          return DropdownMenuItem<String>(
                            value: dropDownStringItem,
                            child: Text(dropDownStringItem),
                          );
                        }).toList(),
                        hint: Text("Selecione uma categoria"),
                        onChanged: (value) {
                          setState(() {
                            valueClassificacao = value;
                          });
                        },
                        isExpanded: true,
                        icon: Icon(
                          Icons.category,
                          color: Colors.red,
                        ),
                        value: valueClassificacao,
                        elevation: 2,
                        iconSize: 20.0,
                      ),
                    ],
                  )),
            ),
            RaisedButton(
              color: Colors.indigo,
              textColor: Colors.white,
              child: Text("AVANÇAR"),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => selecioanrItem()),
                );
              },
            ),
          ],
        )
      ],
    );
  }

  selecioanrItem() {
    var _valueGut;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white, //change your color here
        ),
        title: Text("Novo Chamado",
            style: TextStyle(
              color: Colors.white,
            )),
        elevation: 0.0,
        backgroundColor: Color(0xFFd80645),
        actions: <Widget>[],
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 50, bottom:40),
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Form(
            key: formKey,
            child: Column(
              children: <Widget>[
                Center(
                    child: Container(
                  padding: EdgeInsets.only(left: 40, right: 40),
                  child: Text(
                    "Você está abrindo um novo chamado selecione abaixo o problema\nPasso 2 de 2",
                    textAlign: TextAlign.center,
                  ),
                )),
                Center(
                  child: Container(
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.all(40),
                    child: Column(
                      children: <Widget>[teste()],
                    ),
                  ),
                ),
                Center(
                    child: Container(
                      padding: EdgeInsets.only(right: 20),
                      child: Column(
                        children: <Widget>[
                          //slider
                          Center(child: Text("Em uma escala de 1 a 5\nSendo que: 1 para não muito importante (Pouco) e 5 para muito importante (Bastante), responda os quesitos abaixo\n", style: TextStyle(fontSize: 17), textAlign: TextAlign.center,),),
                          Text("Me impede de trabalhar?"),
                          TextFormField(
                            maxLength: 1,
                            style: TextStyle(color: Colors.black),
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                icon: Icon(
                                  Icons.priority_high,
                                  color: Colors.red,
                                ),
                                labelText: "Informe o valor",
                                labelStyle: TextStyle(color: Colors.black)),
                            controller: gravidadeChamado,
                            validator: (valor) {
                              if (valor.isEmpty) {
                                return "Obrigatório";
                              }
                              return null;
                            },
                          ),
                          Text("É necessário urgência na resolução do problema?"),
                          TextFormField(
                            maxLength: 1,
                            style: TextStyle(color: Colors.black),
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                icon: Icon(
                                  Icons.priority_high,
                                  color: Colors.red,
                                ),
                                labelText: "Informe o valor",
                                labelStyle: TextStyle(color: Colors.black)),
                            controller: urgenciaChamado,
                            validator: (valor) {
                              if (valor.isEmpty) {
                                return "Obrigatório";
                              }
                              return null;
                            },
                          ),
                          Text("Pode piorar?"),
                          TextFormField(
                            maxLength: 1,
                            style: TextStyle(color: Colors.black),
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                icon: Icon(
                                  Icons.priority_high,
                                  color: Colors.red,
                                ),
                                labelText: "Informe o valor",
                                labelStyle: TextStyle(color: Colors.black)),
                            controller: tendenciaChamado,
                            validator: (valor) {
                              if (valor.isEmpty) {
                                return "Obrigatório";
                              }
                              return null;
                            },
                          ),
                        ],
                      ),
                    )
                ),
                Center(
                  child: Container(
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.all(10),
                    child: Column(
                      children: <Widget>[
                        Text(
                            "Se você achar que precisa, poderá comentar abaixo: "),
                        TextFormField(
                          controller: comentario,
                          maxLength: 150,
                          style: TextStyle(color: Colors.black),
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              icon: Icon(
                                Icons.comment,
                                color: Colors.red,
                              ),
                              labelText: "Comentário adicional",
                              labelStyle: TextStyle(color: Colors.black)),
                          // controller: tendencia,
                          validator: (valor) {
                            if (valor.isEmpty) {
                              return "Obrigatório";
                            }
                            return null;
                          },
                        ),
                      ],
                    ),
                  ),
                ),
                RaisedButton(
                  color: Colors.indigo,
                  textColor: Colors.white,
                  child: Text("CONCLUIR"),
                  onPressed: () {
                    int gravidadeint = int.parse(gravidadeItem.toString());
                    int urgenciaint = int.parse(urgenciaItem.toString());
                    int tendenciaint = int.parse(tendenciaItem.toString());

                    var totalGutChamado = int.parse(gravidadeChamado.text) * int.parse(urgenciaChamado.text) * int.parse(tendenciaChamado.text);


                    var totalChamadoItemGut = (gravidadeint * urgenciaint * tendenciaint);


                    var mediaGeralFinal =  (totalChamadoItemGut + totalGutChamado) / 2;
                    if (formKey.currentState.validate()) {
                      //cadastrarItem();
                      if (valueClassificacao == "Computador") {
                        idCategoria = "-LtgGvRcdg-aZ2P4BTVK";
                      } else if (valueClassificacao == "Rede") {
                        idCategoria = "-LtgHPHiAzcZfxvuFe3A";
                      } else if (valueClassificacao == "Periféricos") {
                        idCategoria = "-LtgHiL0YgmTbjQ2jFb3";
                      } else if (valueClassificacao == "Impressoras") {
                        idCategoria = "ExM0mwB6BOcecfLXnEtH";
                      } else if (valueClassificacao == "Móveis") {
                        idCategoria = "skxfL3GG6l9BwXKSuumT";
                      }
                      Firestore.instance.collection("chamado").add({
                        "usuario": "PEGAR_USUARIO_AQUI",
                        "categoria": valueClassificacao,
                        "idCategoria": idCategoria,
                        "itemGUT": _category,
                        "comentario": comentario.text,
                        "dataAbertura": new DateTime.now(),
                        "status": 1,
                        "idItem": iditem,
                        "gravidadeItem": gravidadeItem,
                        "urgenciaItem": urgenciaItem,
                        "tendenciaItem": tendenciaItem,
                        "totalChamadoItemGut": totalChamadoItemGut,
                        "gravidadeChamado": int.parse(gravidadeChamado.text),
                        "urgenciaChamado": int.parse(urgenciaChamado.text),
                        "tendenciaChamado": int.parse(tendenciaChamado.text),
                        "totalGutChamado": totalGutChamado,
                        "mediaGeralFinal": mediaGeralFinal,
                      });
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => concluirChamado()),
                      );
                    }
                  },
                ),
              ],
            ),
          ),
        ],
      )),
    );
  }

  teste() {
    return StreamBuilder<QuerySnapshot>(
        stream: Firestore.instance.collection('itensGUT').snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData)
            return const Center(child: Text("Nada a exibir"));
          var length = snapshot.data.documents.length;
          DocumentSnapshot ds = snapshot.data.documents[length - 1];
          queryCat = snapshot.data.documents;
          return new Container(
            child: new Row(
              children: <Widget>[
                DropdownButton<String>(
                  items:
                      snapshot.data.documents.map((DocumentSnapshot document) {
                    iditem = document.documentID;
                    gravidadeItem = document.data['gravidade'];
                    urgenciaItem = document.data['urgencia'];
                    tendenciaItem = document.data['tendencia'];
                    return new DropdownMenuItem<String>(
                      value: document.data['nomeItem'],
                      child: Text(
                        document.data['nomeItem'],
                        style: TextStyle(color: Colors.black),
                      ),
                    );
                  }).toList(),
                  hint: Text("Selecione uma item"),
                  onChanged: (String newValue) {
                    setState(() {
                      _category = newValue;
                      //print("=====CATEGORY=====" + _category);
                    });
                  },
                  icon: Icon(
                    Icons.category,
                    color: Colors.red,
                  ),
                  value: _category,
                  elevation: 2,
                  iconSize: 20.0,
                ),
              ],
            ),
          );
        });
  }

  concluirChamado() {
    return Scaffold(
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Center(
              child: Text("Muito bem!"),
            ),
            Center(
              child: Text("A solicitação foi enviada"),
            ),
            Center(
              child: RaisedButton(
                color: Colors.transparent,
                elevation: 0,
                child: Text("Voltar a tela Inicial"),
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.pop(context);
                  Navigator.pop(context);
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
