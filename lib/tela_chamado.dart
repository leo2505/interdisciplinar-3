import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';

import 'novo_chamado.dart';

class TelaChamado extends StatefulWidget {
  final DocumentSnapshot item;

  TelaChamado(this.item);

  @override
  _TelaChamadoState createState() => _TelaChamadoState();
}

class _TelaChamadoState extends State<TelaChamado> {
  var valueStatus;
  var tiposStatus = ["Análise", "Andamento", "Cancelado", "Concluído"];

  //GUT DO ITEM
  var GUT;

  //GUT DO USUARIO
  var GUTUsuario;

  //PRIORIDADE FINAL
  var GUTFinal;


  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    return DefaultTabController(
      // Added
      length: 3, // Added
      initialIndex: 0, //Added
      child: Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.white, //change your color here
          ),
          bottom: TabBar(
            indicatorColor: Colors.pinkAccent,
            tabs: [
              Tab(
                child: Text(
                  "Detalhes",
                  style: TextStyle(color: Colors.white),
                ),
              ),
              Tab(
                child: Text(
                  "Respostas",
                  style: TextStyle(color: Colors.white),
                ),
              ),
              Tab(
                child: Text(
                  "Histórico",
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          ),
          title: Text(
            "ITIL Help",
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          elevation: 0.0,
          backgroundColor: Color(0xFFd80645),
          actions: <Widget>[
            FlatButton(
              child: Icon(
                Icons.add,
                color: Colors.white,
                size: 24.0,
                semanticLabel: 'Novo Chamado',
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => NovoChamado()),
                );
              },
            ),
          ],
        ),
        body: TabBarView(
          children: [pagina1(), pagina2(), pagina3()],
        ),
      ),
    );
  }

  pagina1() {
    GUT = widget.item.data["totalChamadoItemGut"];
    GUT = GUT.toString();
    GUTUsuario = widget.item.data["totalGutChamado"];
    GUTUsuario = GUTUsuario.toString();
    GUTFinal = widget.item.data["mediaGeralFinal"];
    GUTFinal = GUTFinal.toString();
    return Container(
      // Add box decoration
      decoration: BoxDecoration(
        // Box decoration takes a gradient
        gradient: LinearGradient(
          // Where the linear gradient begins and ends
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          // Add one stop for each color. Stops should increase from 0 to 1
          colors: [
            // Colors are easy thanks to Flutter's Colors class.
            Color(0xFFd80645),
            Color(0xFFfb6c40),
          ],
        ),
      ),
      child: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(20),
          child: Card(
            // Lista os itens
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Column(
                      children: <Widget>[
                        //Text("Chamado #0001 - GUT ", style:

                        Text(
                          "PM - #" + GUTFinal ?? "",
                          style: TextStyle(fontSize: 20, color: Colors.black38),
                        ),
                      ],
                    ),
                  ),
                  Text(
                    widget.item.data["itemGUT"],
                    style: TextStyle(fontSize: 18, color: Colors.black),
                  ),
                  Text(
                    "Item - " + GUT ?? "",
                  ),
                  Text(
                    "Usuário - " + GUTUsuario ?? "",
                  ),
                  Container(
                      padding: EdgeInsets.all(5.3),
                      child: Text(
                        widget.item.data["comentario"].toString(),
                        style: TextStyle(fontSize: 16),
                      )),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      ButtonBar(
                        alignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          FlatButton(
                            child: const Text('STATUS'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(3.0)),
                            textColor: Colors.black38,
                            onPressed: () {
                              showDialog(
                                  context: context,
                                  builder: (context) {
                                    return AlertDialog(
                                      title: Text('Definir Status'),
                                      content: DropdownButton<String>(
                                        items: tiposStatus
                                            .map((String dropDownStringItem) {
                                          return DropdownMenuItem<String>(
                                            value: dropDownStringItem,
                                            child: Text(dropDownStringItem),
                                          );
                                        }).toList(),
                                        hint: Text("Selecione uma status"),
                                        onChanged: (value) {
                                          setState(() {
                                            valueStatus = value;
                                          });
                                        },
                                        isExpanded: true,
                                        icon: Icon(
                                          Icons.category,
                                          color: Colors.red,
                                        ),
                                        value: valueStatus,
                                        elevation: 2,
                                        iconSize: 20.0,
                                      ),
                                      actions: <Widget>[
                                        FlatButton(
                                          child: new Text('OK'),
                                          onPressed: () {
                                            if (valueStatus == "Análise") {
                                              Firestore.instance
                                                  .collection("chamado")
                                                  .document(
                                                  widget.item.documentID)
                                                  .updateData({
                                                "dataAlteracaoStatus":
                                                new DateTime.now(),
                                                "nomestatus": "Em Análise",
                                                "status": 2
                                              });
                                            } else if (valueStatus ==
                                                "Andamento") {
                                              Firestore.instance
                                                  .collection("chamado")
                                                  .document(
                                                  widget.item.documentID)
                                                  .updateData({
                                                "dataAlteracaoStatus":
                                                new DateTime.now(),
                                                "nomestatus": "Em Andamento",
                                                "status": 2
                                              });
                                            } else if (valueStatus ==
                                                "Cancelado") {
                                              Firestore.instance
                                                  .collection("chamado")
                                                  .document(
                                                  widget.item.documentID)
                                                  .updateData({
                                                "dataAlteracaoStatus":
                                                new DateTime.now(),
                                                "nomestatus": "Cancelado",
                                                "status": 3
                                              });
                                            } else if (valueStatus ==
                                                "Concluído") {
                                              Firestore.instance
                                                  .collection("chamado")
                                                  .document(
                                                  widget.item.documentID)
                                                  .updateData({
                                                "dataAlteracaoStatus":
                                                new DateTime.now(),
                                                "nomestatus": "Concluído",
                                                "status": 3
                                              });
                                            }

                                            Navigator.of(context).pop();
                                          },
                                        ),
                                      ],
                                    );
                                  });
                            },
                          ),
                          FlatButton(
                            color: Colors.indigo,
                            child: const Text('FINALIZAR'),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(3.0)),
                            textColor: Colors.white,
                            onPressed: () {
                              Firestore.instance
                                  .collection("chamado")
                                  .document(widget.item.documentID)
                                  .updateData({
                                "dataAlteracaoStatus": new DateTime.now(),
                                "nomestatus": "Concluído",
                                "status": 3
                              });
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      )
    );
  }

  pagina2() {
    return Container(
      // Add box decoration
      decoration: BoxDecoration(
        // Box decoration takes a gradient
        gradient: LinearGradient(
          // Where the linear gradient begins and ends
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          // Add one stop for each color. Stops should increase from 0 to 1
          colors: [
            // Colors are easy thanks to Flutter's Colors class.
            Color(0xFFd80645),
            Color(0xFFfb6c40),
          ],
        ),
      ),
      child: Container(),
    );
  }

  pagina3() {
    var nomestatus = widget.item.data["nomestatus"];
    nomestatus == null ? " " : widget.item.data["nomestatus"];

    var dataalteracaochamado = widget.item.data["dataAlteracaoStatus"];
    dataalteracaochamado == null
        ? " "
        : dataalteracaochamado = formatDate(
            widget.item.data["dataAlteracaoStatus"].toDate(),
            [dd, '/', mm, '/', yyyy, "  ", HH, ":", nn]);
    return Container(
      // Add box decoration
      decoration: BoxDecoration(
        // Box decoration takes a gradient
        gradient: LinearGradient(
          // Where the linear gradient begins and ends
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          // Add one stop for each color. Stops should increase from 0 to 1
          colors: [
            // Colors are easy thanks to Flutter's Colors class.
            Color(0xFFd80645),
            Color(0xFFfb6c40),
          ],
        ),
      ),
      child: Container(
        padding: EdgeInsets.all(5.3),
        margin: EdgeInsets.only(top: 10),
        child: Column(
          children: <Widget>[

            Center(
              child: Text(
                  "Aberto\n" +
                      formatDate(widget.item.data["dataAbertura"].toDate(),
                          [dd, '/', mm, '/', yyyy, "  ", HH, ":", nn]),
                  textAlign: TextAlign.center, style: TextStyle(color: Colors.white),),
            ),
            Text(
              "\n",
            ),
            Center(
              child: Text(
                "Status Atual\n(" +
                    nomestatus.toString() +
                    ")\n" +
                    dataalteracaochamado.toString(),
                textAlign: TextAlign.center,style: TextStyle(color: Colors.white)
              ),
            )
          ],
        ),
      ),
    );
  }
}
