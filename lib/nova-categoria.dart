import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';


class NovaCategoria extends StatefulWidget {
  DocumentSnapshot dadosBranco;
  @override
  _NovaCategoriaState createState() => _NovaCategoriaState();
}

class _NovaCategoriaState extends State<NovaCategoria> {

  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController nomeCategoria = TextEditingController();
  TextEditingController gravidade = TextEditingController();
  TextEditingController tendencia = TextEditingController();
  TextEditingController urgencia = TextEditingController();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.blue, //change your color here
          ),
          title: Text("Nova Categoria",
              style: TextStyle(
                color: Colors.blue,
              )
          ),
          elevation: 0.0,
          backgroundColor: Colors.white,
          actions: <Widget>[
            FlatButton(
              child: Icon(
                Icons.add,
                color: Colors.blue,
                size: 24.0,
                semanticLabel: 'NOVO CHAMADO',
              ),
              onPressed: (){

              },
            )
          ],
        ),
        body: novaCategoria()
    );
  }
  novaCategoria(){
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.all(10),
        child:  Form(
          key: formKey,
          child: Column(
            children: <Widget>[
              TextFormField(
                style: TextStyle(color: Colors.black),
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                    icon: Icon(Icons.category, color: Colors.red,),
                    labelText: "Nome da Categoria", labelStyle: TextStyle(color: Colors.black)),
                controller: nomeCategoria,
                validator: (valor){
                  if(valor.isEmpty){
                    return "Informe o nome da categoria";
                  }
                  return null;
                },
              ),
              TextFormField(
                maxLength: 1,
                style: TextStyle(color: Colors.black),
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    icon: Icon(Icons.priority_high, color: Colors.red,),
                    labelText: "Gravidade", labelStyle: TextStyle(color: Colors.black)),
                controller: gravidade,
                validator: (valor){
                  if(valor.isEmpty){
                    return "Obrigatório";
                  }
                  return null;
                },
              ),
              TextFormField(
                maxLength: 1,
                style: TextStyle(color: Colors.black),
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    icon: Icon(Icons.priority_high, color: Colors.red,),
                    labelText: "Urgência", labelStyle: TextStyle(color: Colors.black)),
                controller: urgencia,
                validator: (valor){
                  if(valor.isEmpty){
                    return "Obrigatório";
                  }
                  return null;
                },
              ),
              TextFormField(
                maxLength: 1,
                style: TextStyle(color: Colors.black),
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    icon: Icon(Icons.priority_high, color: Colors.red,),
                    labelText: "Tendência", labelStyle: TextStyle(color: Colors.black)),
                controller: tendencia,
                validator: (valor){
                  if(valor.isEmpty){
                    return "Obrigatório";
                  }
                  return null;
                },
              ),
              RaisedButton(
                color: Colors.red,
                textColor: Colors.white,
                child: Text("Cadastrar Categoria"),
                onPressed: (){
                  if(formKey.currentState.validate()){
                    cadastrarCategoria();
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
  cadastrarCategoria(){

  }
}
