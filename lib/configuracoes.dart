import 'package:flutter/material.dart';
import 'package:interdisciplinar_app/lista_itens.dart';
import 'package:interdisciplinar_app/nova-categoria.dart';
import 'package:interdisciplinar_app/novo-item-gut.dart';

class Configuracoes extends StatefulWidget {
  @override
  _ConfiguracoesState createState() => _ConfiguracoesState();
}

class _ConfiguracoesState extends State<Configuracoes> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor:Color(0xFFd80645),
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.white, //change your color here
          ),
          title: Text("Configuração",
              style: TextStyle(
                color: Colors.white,
              )
          ),
          elevation: 0.0,
          backgroundColor: Color(0xFFd80645),
          actions: <Widget>[

          ],
        ),
        body: telaConfig()
    );
  }
  telaConfig(){
    return Container(
      color: Color(0xFFd80645),
      padding: EdgeInsets.all(0),
      alignment: Alignment.center,
      child: ListView(
        children: <Widget>[

              RaisedButton(
                  color: Color.fromRGBO(255, 255, 255, 0.2),
                  elevation: 0,
                  textColor: Colors.white,
                  child: Text("Novo item GUT"),
                  onPressed: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => NovoItemGut()),
                    );
                  }
              ),
              RaisedButton(
                  color: Color.fromRGBO(255, 255, 255, 0.2),
                  elevation: 0,
                  textColor: Colors.white,
                  child: Text("Itens Cadastrados"),
                  onPressed: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => ListaItens()));
                  }
              ),
          RaisedButton(
              color: Color.fromRGBO(255, 255, 255, 0.2),
              elevation: 0,
              textColor: Colors.white,
              child: Text("Sair do App"),
              onPressed: (){
                Navigator.pop(context);
                Navigator.pop(context);
              }
          ),
            ],
          ),
    );
  }
}
