import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class ListaItens extends StatefulWidget {
  @override
  _ListaItensState createState() => _ListaItensState();
}

class _ListaItensState extends State<ListaItens> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFFd80645),
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.white, //change your color here
          ),
          title: Text("Todos os Itens",
              style: TextStyle(
                color: Colors.white,
              )
          ),
          elevation: 0.0,
          backgroundColor: Color(0xFFd80645),
          actions: <Widget>[

          ],
        ),
        body: listaItens()
    );
  }
  listaItens(){
    return  Container(
        child: StreamBuilder(
            stream: Firestore.instance
                .collection("itensGUT")
                .orderBy("nomeItem")
                .snapshots(),
            builder: (context, snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.none:
                case ConnectionState.done:
                case ConnectionState.waiting:
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                default:
                  if (snapshot.data.documents.length == 0) {
                    //
                    return Center(
                      child: Text(
                        "Não há dados! " + snapshot.error.toString(),
                        style: TextStyle(
                            color: Colors.redAccent, fontSize: 20),
                      ),
                    );
                  }

                  return ListView.builder(
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (context, index) {
                        return Card(
                          // Lista os itens
                          elevation: 0,
                          color: Color.fromRGBO(255, 255, 255, 0.25),
                          child: Container(
                            child: ListTile(
                              //snapshot.data.documents[index].documentID.toString() - pega o ID
                              title: Text(
                                  snapshot.data.documents[index]
                                      .data["nomeItem"],
                                  style: TextStyle(
                                      fontSize: 18, color: Colors.white)),
                              subtitle: Text("Categoria: " + snapshot.data.documents[index]
                                  .data["nomeCategoria"]
                                  .toString(), style: TextStyle(fontSize: 13, color: Colors.white)),
                              trailing: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  IconButton(
                                    icon: Icon(Icons.edit),
                                    color: Colors.white,
                                    onPressed: () {
                                      //Navigator.push(
                                      //    context, MaterialPageRoute(builder: (context) => FilmesEditar("alt",snapshot.data.documents[index])));
                                    },
                                  ),
                                  IconButton(
                                    icon: Icon(Icons.delete),
                                    color: Colors.white70,
                                    onPressed: () {
                                      confirmaExclusao(context, index, snapshot);
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      });
              }
            }),
    );
  }
  confirmaExclusao(BuildContext context, index, snapshot) {
    showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Atenção !"),
          content: Text("Confirma a exclusão de : \n" +
              snapshot.data.documents[index].data["nomeItem"]
                  .toString()
                  .toUpperCase()),
          actions: <Widget>[
            FlatButton(
              child: Text('Cancelar'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Firestore.instance
                    .collection('itensGUT')
                    .document(
                    snapshot.data.documents[index].documentID.toString())
                    .delete();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
